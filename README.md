# prototype

unified messages:
    id: _id
    content: str
    contenttype: str
    datetime: Date
    channel: 
        id: str
        name: str
    sender:
        id: str
        name: str
    platform: str
    user: str
    priority:
        range[0,1]

feathers:
    users:
    id: _id
    <!-- email: str -->
    password: str
    username: str
    first_name: str
    last_name: str
    platforms: [
        name: str
        loggin…
    ]
    channels:[
        id: _id,
        name: str
    ]



<!-- channels.usesr_id:
 id: _id
 name: str
 source_channels: [
  platform…
 ] -->

channels
    _id
    users: []
    name
    platform:
        name
        channel

contacts.user_id: …

Queue functions
    status
    add user
    remove user
    publish message


> test server for amy

## About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/prototype; npm install
    ```

3. Start your app

    ```
    npm start
    ```

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers help                           # Show all commands
```

## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).

## Changelog

__0.1.0__

- Initial release

## License

Copyright (c) 2018

Licensed under the [MIT license](LICENSE).
