PORTS ?= -p 3030:3030
IMAGE_NAME ?= registry.gitlab.com/amy-assistant/prototype
CONTAINER_NAME ?= amy-prototype
LINKS ?= --link rabbitmq --link mongodb

build: Dockerfile
	docker build -t $(IMAGE_NAME) .

push:
	docker push $(IMAGE_NAME)

run:
	docker run --rm -it --name $(CONTAINER_NAME) $(PORTS) $(LINKS) $(IMAGE_NAME)

start:
	docker run -d --name $(CONTAINER_NAME) $(PORTS) $(IMAGE_NAME)

stop:
	docker stop $(CONTAINER_NAME)

shell:
	docker exec -it $(CONTAINER_NAME) sh

rm:
	docker rm $(CONTAINER_NAME)