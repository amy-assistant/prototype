const {ObjectId} = require('mongodb')

const fixObjId = input => ctx => {
    const path = input.split('.')
    const end = path.pop()
    const ref =  path.reduce((obj, key) => obj[key] || obj, ctx) || ctx
    if (ref[end]) ref[end] = ObjectId(ref[end])
  }

module.exports = {
    fixObjId
}