module.exports = collection => async context => {
  const db = await context.app.get("mongoClient");
  if (context.params.user)
    context.service.Model = db.collection(
      `${collection}.${context.params.user._id}`
    );
  return context;
};
