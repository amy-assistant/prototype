const jackrabbit = require('jackrabbit');


module.exports = app => {
  const config = app.get('rabbitmq');
  console.log('rabbit connecting to ',config)

  app.set('rabbitClient', jackrabbit(config));
};
