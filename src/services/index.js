const users = require('./users/users.service');
const messages = require('./messages/messages.service');
const channels = require('./channels/channels.service');
const plugins = require('./plugins/plugins.service.js');
const contacts = require('./contacts/contacts.service.js');

// eslint-disable-next-line no-unused-vars
module.exports = (app) => {
  app.configure(users);
  app.configure(messages);
  app.configure(channels);
  app.configure(plugins);
  app.configure(contacts);
};
