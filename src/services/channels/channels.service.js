// Initializes the `channels` service on path `/channels`
const createService = require('feathers-mongodb');
const hooks = require('./channels.hooks');

module.exports = function (app) {
  const paginate = app.get('paginate');
  const mongoClient = app.get('mongoClient');
  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/channels', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('channels');

  mongoClient.then(db => {
    service.Model = db.collection('channels');
  }).catch(console.error);

  service.hooks(hooks);
  service.publish((data, context) => context.params.user && app.channel(`${context.params.user._id}`)
  );

};
