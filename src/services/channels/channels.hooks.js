const { authenticate } = require('@feathersjs/authentication').hooks;
const {fixObjId} = require('../../hooks/hooks')


const {
  findOrCreate,
  populateLastMessage
} = require('./channelMethods');

const {changeModel} = require('../../hooks');
// const {fixObjID} = require('../../hooks/hooks')

module.exports = {
  before: {  
    all: [ authenticate('jwt'), changeModel('channels')],
    find: [],
    get: [],
    create: [findOrCreate],
    update: [],
    patch: [fixObjId('id')],
    remove: []
  },

  after: {
    all: [populateLastMessage],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
