const {
  populate
} = require('feathers-hooks-common');

const findOrCreate = async context => {
  const service = context.service;
  const channel = context.data;

  const f = Object.keys(channel.platforms)[0];

  const params = {
    query: {
      [`platforms.${f}`]: channel.platforms[f]
    }
  };
  try{
    const response = await service.find(params);

    if (response.length || response.data && response.data.length) {
      // Set the result to skip the call to `create` if a record was found.
      context.result = response.data ? response.data[0] : response[0];
    }
    // console.log(context.result)

    return context;
  }
  catch(error){
    console.error(error)
  }
 
}


const lastMessageSchema = {
  include: {
    service: 'messages',
    nameAs: 'lastMessage',
    parentField: '_id',
    childField: 'channel',
    query: {
      $limit: 1,
      $select: ['content', 'datetime'],
      $sort: {
        datetime: -1
      }
    },
  }
};

module.exports = {
  findOrCreate,
  populateLastMessage: populate({
    schema: lastMessageSchema
  })
};
