/* eslint-disable no-unused-vars */
const { action, checkData } = require("./pluginsMethods");

function plugin(data) {
  let plugin = {};
  if ("username" in data) plugin.username = data.username;
  if ("session" in data) plugin.session = data.session;
  return plugin;
}

class Service {
  constructor(options) {
    this.options = options || {};
  }

  setup(app, path) {
    this.app = app;
    this.users = this.app.service("users");
    this.rabbit = app.get("rabbitClient").default();
  }

  async find(params) {
    return params.user.platforms;
  }

  async get(id, params) {
    return params.user.platforms[id];
  }

  async create(data, params) {
    return data;
  }

  async update(id, data, params) {
    return data;
  }

  async patch(method, data, params) {
    try {
      const { username, platform } = checkData(params, data);

      // Status
      if (method == "status") {
        return await action(this.rabbit, "status", { platform }, username);
      }
      // Load
      else if (method == "load") {
        const res = await action(
          this.rabbit,
          "create",
          { platform, session: data.session },
          username
        );
        if (res.status == 400) {
          await this.users.update(params.user._id, {
            $set: {
              [`platforms.${platform}.session`]: undefined
            }
          });
        }
        return res;
      }
      // Create
      else if (method == "create") {
        const res = await action(this.rabbit, "create", data, username);

        await this.users.update(params.user._id, {
          $set: {
            [`platforms.${platform}`]: plugin(data)
          }
        });
        return res;
      }
      // Authorize
      else if (method == "authorize") {
        const res = await action(this.rabbit, "authorize", data, username);
        let update = {
          ...plugin(res),
          ...params.user.platforms[platform]
        };

        await this.users.update(params.user._id, {
          $set: {
            [`platforms.${platform}`]: update
          }
        });
        return res;
      } else if (method == "send") {
        return await action(this.rabbit, "send", data, username);
      }
    } catch (error) {
      console.error(error);
    }
  }

  async remove(platform, params) {
    try {
      await action(
        this.rabbit,
        "remove",
        { platform },
        params.user.platforms[platform].username
      );
      return await this.users.update(params.user._id, {
        $unset: {
          [`platforms.${platform}`]: 1
        }
      });
    } catch (error) {
      console.error(error);
    }
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
