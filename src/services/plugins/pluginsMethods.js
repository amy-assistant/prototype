const { asyncRPC } = require("../../methods/pluginRabbit");

const setupQueue = context => {
  if (context.data.platform) {
    const exch = context.app.get("rabbitClient").default();
    exch.queue({
      name: context.data.platform,
      durable: true
    });
  }
  return context;
};

async function action(exchange, method, data, username) {
  try {
    const res = await asyncRPC(exchange, data.platform, data, {
      action: method,
      username
    });
    if (internalError(res.status)) return res;
  } catch (error) {
    console.error(error);
  }
}

function internalError(status) {
  return 200 <= status && status < 500 ? true : false;
}

function checkData(params, data) {
  let username =
    params.user &&
    params.user.platforms &&
    params.user.platforms[data.platform] &&
    params.user.platforms[data.platform].username
      ? params.user.platforms[data.platform].username
      : data.username;
  if (!username) throw new Error("Needs username");

  let platform = data.platform;
  if (!platform) throw new Error("Needs platfrom");

  return { username, platform };
}

module.exports = {
  setupQueue,
  internalError,
  action,
  checkData
};
