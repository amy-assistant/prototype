const {
  populate
} = require('feathers-hooks-common');


const filterMessageChannel = async context => {
  try {
    let res = await context.app.service("channels").find(context.params);

    res = res.data.reduce((ids, channel) => [...ids, channel._id], []);

    context.params.query = {
      channel: {
        $in: res
      }
    };

    return context;
  } catch (error) {
    console.error(error);
  }
};

const attachChannel = async context => {
  try {
    if (context.params.listener) {
      const channels = context.app.service("channels");
      const user = context.params.user;
      const message = context.data;
      message.channel = (await channels.create(
        {
          name: message.channel,
          platforms: {
            [message.platform]: message.channel
          }
        },
        {
          user
        }
      ))._id;
      message.user = user._id;
      message.datetime = new Date(message.datetime);
    }
    return context;
  } catch (error) {
    console.error(error);
  }
};

const attachContact = async context => {
  try {
    if (context.params.listener && !context.data.out) {
      const contacts = context.app.service("contacts");
      const user = context.params.user;
      const message = context.data;
      message.sender = (await contacts.create(
        {
          name: message.sender,
          platforms: {
            [message.platform]: message.sender
          }
        },
        {
          user
        }
      ))._id;
      message.user = user._id;
      message.datetime = new Date(message.datetime);
    }
    return context;
  } catch (error) {
    console.error(error);
  }
};

// const { ObjectId } = require("mongodb");

// const patchChannelId = context => {
//   if (context.params.query.channel)
//     context.params.query.channel = ObjectId(context.params.query.channel);
//   return context;
// };

const senderName = {
  include: {
    service: 'contacts',
    nameAs: 'sender',
    parentField: 'sender',
    childField: '_id',
    query: {
      $limit: 1,
      $select: ['name'],
    },
  }
};


module.exports = {
  filterMessageChannel,
  attachChannel,
  attachContact,
  populateSenderName: populate({
    schema: senderName
  })
  // patchChannelId
};
