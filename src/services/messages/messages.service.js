// Initializes the `messages` service on path `/messages`
const createService = require('feathers-mongodb');
const hooks = require('./messages.hooks');

module.exports = app => {
  const paginate = app.get('paginate');
  const mongoClient = app.get('mongoClient');
  const options = {
    paginate,
    multi: ['patch']
  };

  // Initialize our service with any options it requires
  app.use('/messages', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('messages');

  mongoClient.then(db => {
    service.Model = db.collection('messages');
  }).catch(console.error);

  service.hooks(hooks);
  service.publish((data, context) => context.params.user && app.channel(`${context.params.user._id}`));
};
