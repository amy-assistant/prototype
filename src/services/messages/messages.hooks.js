const { authenticate } = require("@feathersjs/authentication").hooks;

const { fixObjId } = require("../../hooks/hooks");

const {
  attachChannel,
  attachContact,
  populateSenderName
  // patchChannelId
} = require("./messageMethods");

const { changeModel } = require("../../hooks");

module.exports = {
  before: {
    all: [authenticate("jwt"), changeModel("messages")],
    find: [fixObjId("params.query.channel"), fixObjId("params.query.sender")],
    get: [],
    create: [attachChannel, attachContact],
    update: [],
    patch: [
      fixObjId("data.channel"),
      fixObjId("data.sender"),
      fixObjId("params.query.channel"),
      fixObjId("params.query.sender")
    ],
    remove: []
  },

  after: {
    all: [],
    find: [populateSenderName],
    get: [populateSenderName],
    create: [populateSenderName],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
