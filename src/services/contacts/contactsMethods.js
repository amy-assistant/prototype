const findOrCreate = async context => {
  const service = context.service;
  const contact = context.data;

  const f = Object.keys(contact.platforms)[0];

  const params = {
    query: {
      [`platforms.${f}`]: contact.platforms[f]
    }
  };
  try{
    const response = await service.find(params);

    if (response.length || response.data && response.data.length) {
      // Set the result to skip the call to `create` if a record was found.
      context.result = response.data ? response.data[0] : response[0];
    }
    // console.log(context.result)

    return context;
  }
  catch(error){
    console.error(error)
  }
 
}



module.exports = {
  findOrCreate,
};
