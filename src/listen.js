module.exports = app => {
  const rabbitClient = app.get('rabbitClient');
  const messageServive = app.service('messages');
  const userService = app.service('users');
  const pluginService = app.service('plugins');

  // new messages
  rabbitClient
    .default()
    .queue({
      name: app.get('message_q'),
      durable: true
    })
    .consume((data, ack) => {
      if (!data) return;
      userService.find({
        query: {
          [`platforms.${data.platform}.username`]: data.user
        }
      }).then(res => res[0]).then(user => {
        messageServive.create(data, {
          user,
          listener: true
        });
        ack()
      }).catch(console.error);
    });

  // init plugin and auth user
  rabbitClient
    .default()
    .queue({
      name: app.get('plugins_q'),
      durable: true
    })
    .consume((data, ack) => {
      userService.find({
        query: {
          [`platforms.${data.name}.session`]: {
            $ne: undefined
          },
        }
      }).then(users => {

        for (let user of users) {
          pluginService.patch('load', {
            platform: data.name,
            session: user.platforms[data.name].session,
            username: user.platforms[data.name].username
          }, {
            user
          })
        }
        ack()
      }).catch(console.error)
    });
};