function asyncRPC(exchange, queue, body, headers = {}) {
  return new Promise((res, rej) => {
    try {
      exchange.publish(body, {
        key: queue,
        reply: res,
        headers
      });
    } catch(error) {
      rej(error);
    }
  });
}

module.exports = {
  asyncRPC,
};
