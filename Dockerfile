FROM node:11.0.0

WORKDIR /app

COPY . .

ENV NODE_ENV='production'
ENV PORT='3030'
ENV AMY_DB_HOST='mongodb://159.89.24.207:27017/amy'
ENV AMY_Q_HOST='amqp://159.89.24.207'



RUN yarn

CMD ["yarn", "start"]